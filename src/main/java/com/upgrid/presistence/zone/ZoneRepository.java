package com.upgrid.presistence.zone;

import com.upgrid.presistence.core.SearchByNameRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(excerptProjection = ZoneProjection.class)
public interface ZoneRepository extends SearchByNameRepository<Zone, Long> {
}