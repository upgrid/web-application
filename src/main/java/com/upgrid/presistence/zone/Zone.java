package com.upgrid.presistence.zone;

import com.upgrid.presistence.door.Door;
import com.upgrid.presistence.personGroup.PersonGroup;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import java.io.Serializable;
import java.util.Collection;

@Entity
@Data
public class Zone implements Serializable {
    @Id
    @GeneratedValue
    private long id;
    private String name;
    private String type;
    @ManyToMany(mappedBy = "zones")
    private Collection<PersonGroup> personGroups;
    @ManyToMany(mappedBy = "zones")
    private Collection<Door> doors;
}
