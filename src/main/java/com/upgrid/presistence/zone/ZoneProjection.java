package com.upgrid.presistence.zone;

import com.upgrid.presistence.core.OnlyNameProjection;
import org.springframework.data.rest.core.config.Projection;

import java.util.Collection;

@Projection(name = "inlineDoorsAndPersonGroups", types = {Zone.class})
public interface ZoneProjection {
    String getName();

    Collection<OnlyNameProjection> getDoors();

    Collection<OnlyNameProjection> getPersonGroups();
}