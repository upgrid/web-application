package com.upgrid.presistence.doorLog;

import com.upgrid.presistence.door.Door;
import com.upgrid.presistence.idCard.IdCard;
import com.upgrid.presistence.person.Person;
import com.upgrid.presistence.tag.Tag;
import lombok.Data;
import org.hibernate.annotations.NaturalId;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.io.Serializable;

@Entity
@Data
public class DoorLog implements Serializable {

    @Id
    @GeneratedValue
    private long id;
    @ManyToOne
    @NaturalId
    private Person person;
    @ManyToOne
    @NaturalId
    private Tag tag;
    @ManyToOne
    @NaturalId
    private IdCard idcard;
    private Integer level;
    private String message;
    @ManyToOne
    private Door door;
}
