package com.upgrid.presistence.doorLog;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;


@RepositoryRestResource(excerptProjection = DoorLogProjection.class)
public interface DoorLogRepository extends CrudRepository<DoorLog, Long> {
}