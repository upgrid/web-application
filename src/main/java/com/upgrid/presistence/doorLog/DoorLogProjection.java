package com.upgrid.presistence.doorLog;

import com.upgrid.presistence.core.InlinePersonNameProjection;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

@Projection(name = "inlinePersonData", types = {DoorLog.class})
public interface DoorLogProjection extends InlinePersonNameProjection {


    Integer getLevel();


    String getMessage();


    @Value("#{target.tag == null ? '' : target.tag.uid}")
    String getUid();

    @Value("#{target.idcard == null ? '' : target.idcard.document}")
    String getDocument();

    @Value("#{target.door == null ? '' : target.door.name}")
    String getDoor();
}