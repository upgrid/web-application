package com.upgrid.presistence.idCard;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

@Projection(name = "olnyName", types = {IdCard.class})
public interface IdCardDocumentAsNameProjection {
    @Value("#{target.document == null ? 'null' : target.document}")
    String getName();
}
