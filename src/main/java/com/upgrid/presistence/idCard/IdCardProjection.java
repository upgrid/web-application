package com.upgrid.presistence.idCard;

import com.upgrid.presistence.core.InlinePersonNameProjection;
import com.upgrid.presistence.person.PersonFullNameProjection;
import org.springframework.data.rest.core.config.Projection;

@Projection(name = "inlinePersonName", types = {IdCard.class})
public interface IdCardProjection extends InlinePersonNameProjection {
    String getDocument();

    Boolean getActive();

    PersonFullNameProjection getPerson();
}
