package com.upgrid.presistence.idCard;

import com.upgrid.presistence.core.OnlyIdProjection;

public interface OnlyDocumentProjection extends OnlyIdProjection {
    String getDocument();
}
