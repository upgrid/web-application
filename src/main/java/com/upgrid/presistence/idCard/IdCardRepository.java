package com.upgrid.presistence.idCard;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(excerptProjection = IdCardProjection.class)
public interface IdCardRepository extends CrudRepository<IdCard, Long> {
}