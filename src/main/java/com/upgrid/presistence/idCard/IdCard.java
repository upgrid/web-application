package com.upgrid.presistence.idCard;

import com.upgrid.presistence.doorLog.DoorLog;
import com.upgrid.presistence.person.Person;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;

@Entity
@Data
public class IdCard implements Serializable {
    @Id
    @GeneratedValue
    private long id;
    private String document;

    private boolean active;
    @ManyToOne
    private Person person;
    @OneToMany(mappedBy = "idcard")
    private Collection<DoorLog> doorLog;
}
