package com.upgrid.presistence.core;


public interface OnlyIdProjection {
    Long getId();
}