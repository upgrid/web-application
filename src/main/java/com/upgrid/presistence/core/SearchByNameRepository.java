package com.upgrid.presistence.core;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import java.io.Serializable;
import java.util.List;

@NoRepositoryBean
@RepositoryRestResource(excerptProjection = OnlyNameProjection.class)
public interface SearchByNameRepository<T, ID extends Serializable> extends CrudRepository<T, Long> {
    @RestResource(path = "names", rel = "names")
    List<T> findAllByNameContaining(@Param("name") String name);
}