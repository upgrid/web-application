package com.upgrid.presistence.core;

import com.upgrid.presistence.door.Door;
import com.upgrid.presistence.personGroup.PersonGroup;
import com.upgrid.presistence.tag.Tag;
import com.upgrid.presistence.zone.Zone;
import org.springframework.data.rest.core.config.Projection;

@Projection(name = "olnyName", types = {PersonGroup.class, Tag.class, Zone.class, Door.class})
public interface OnlyNameProjection {
    String getName();
}
