package com.upgrid.presistence.core;

import com.upgrid.presistence.doorLog.DoorLog;
import com.upgrid.presistence.idCard.IdCard;
import com.upgrid.presistence.tag.Tag;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

@SuppressWarnings({"SpringElInspection", "ELValidationInJSP"})
@Projection(name = "inlinePersonName", types = {IdCard.class, Tag.class, DoorLog.class})
public interface InlinePersonNameProjection {
    @Value("#{target.person == null ? '' : target.person.lastName}")
    String getPersonLastName();

    @Value("#{target.person == null ? '' : target.person.firstName}")
    String getPersonFirstName();
}
