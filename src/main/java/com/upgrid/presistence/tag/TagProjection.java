package com.upgrid.presistence.tag;

import com.upgrid.presistence.core.InlinePersonNameProjection;
import com.upgrid.presistence.person.PersonFullNameProjection;
import org.springframework.data.rest.core.config.Projection;

@Projection(name = "inlinePersons", types = {Tag.class})
public interface TagProjection extends InlinePersonNameProjection {
    String getName();

    String getUid();

    Boolean getActive();

    PersonFullNameProjection getPerson();
}
