package com.upgrid.presistence.tag;

import com.upgrid.presistence.core.OnlyIdProjection;

public interface OnlyUidProjection extends OnlyIdProjection {
    String getUid();
}