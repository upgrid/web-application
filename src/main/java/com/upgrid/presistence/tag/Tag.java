package com.upgrid.presistence.tag;

import com.upgrid.presistence.doorLog.DoorLog;
import com.upgrid.presistence.person.Person;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;

@Entity
@Data
public class Tag implements Serializable {

    @Id
    @GeneratedValue

    private long id;

    private String uid;

    private String name;

    private boolean active;
    @ManyToOne
    private Person person;
    @OneToMany(mappedBy = "tag")
    private Collection<DoorLog> doorLog;
}
