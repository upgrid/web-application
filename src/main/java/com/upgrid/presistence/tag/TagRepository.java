package com.upgrid.presistence.tag;

import com.upgrid.presistence.core.SearchByNameRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(excerptProjection = TagProjection.class)
public interface TagRepository extends SearchByNameRepository<Tag, Long> {
}
