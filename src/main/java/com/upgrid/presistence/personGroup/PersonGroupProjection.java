package com.upgrid.presistence.personGroup;

import com.upgrid.presistence.core.OnlyNameProjection;
import com.upgrid.presistence.person.PersonFullNameProjection;
import org.springframework.data.rest.core.config.Projection;

import java.util.Collection;

@Projection(name = "inlineData", types = {PersonGroup.class})
public interface PersonGroupProjection {
    String getName();

    Collection<PersonFullNameProjection> getPersons();

    Collection<OnlyNameProjection> getZones();
}
