package com.upgrid.presistence.personGroup;

import com.upgrid.presistence.person.Person;
import com.upgrid.presistence.zone.Zone;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import java.io.Serializable;
import java.util.Collection;

@Entity
@Data
public class PersonGroup implements Serializable {
    @Id
    @GeneratedValue
    private long id;
    private String name;
    @ManyToMany
    private Collection<Zone> zones;
    @ManyToMany(mappedBy = "personGroups")
    private Collection<Person> persons;
}
