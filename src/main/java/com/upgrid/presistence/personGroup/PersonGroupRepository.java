package com.upgrid.presistence.personGroup;

import com.upgrid.presistence.core.SearchByNameRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(excerptProjection = PersonGroupProjection.class)
public interface PersonGroupRepository extends SearchByNameRepository<PersonGroup, Long> {
}