package com.upgrid.presistence.person;


import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import java.util.List;

@RepositoryRestResource(excerptProjection = PersonProjection.class)
public interface PersonRepository extends CrudRepository<Person, Long> {
    @RestResource(path = "names", rel = "names")
    @Query("select p from Person p where concat(p.firstName, ' ', p.lastName)  like concat('%', ?1, '%') ")
    List<Person> findByName(@Param("name") String name);

    //    @Query("select p from Person p, PersonGroup ps, Zone zs, Door ds left join Person.personGroups pg  join pg.zones z join z.doors d where d.id = ?1 GROUP BY p.id")
    @Query("select p from Person p, PersonGroup g, Zone z, Door d where d.id = ?1 GROUP BY p.id")
    List<Person> findAllByDoor(@Param("id") Long door);
}
