package com.upgrid.presistence.person;

import com.upgrid.presistence.core.OnlyNameProjection;
import com.upgrid.presistence.idCard.IdCardDocumentAsNameProjection;
import com.upgrid.presistence.personGroup.PersonGroup;
import org.springframework.data.rest.core.config.Projection;

import java.util.Collection;

@Projection(name = "inlineTagsAndCards", types = {Person.class})
public interface PersonProjection {

    String getFirstName();

    String getLastName();

    Integer getIdCode();

    Boolean getActive();

    Collection<IdCardDocumentAsNameProjection> getIdCards();

    Collection<OnlyNameProjection> getTags();

    Collection<PersonGroup> getPersonGroups();
}