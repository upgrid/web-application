package com.upgrid.presistence.person;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

@Projection(name = "olnyName", types = {Person.class})
public interface PersonFullNameProjection {

    //TODO: Tühja andmebaasi korral saadakse error 500
    @Value("#{target.firstName} #{target.lastName}")
    String getName();
}
