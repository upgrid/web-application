package com.upgrid.presistence.person;

import com.upgrid.presistence.core.OnlyIdProjection;
import com.upgrid.presistence.idCard.OnlyDocumentProjection;
import com.upgrid.presistence.tag.OnlyUidProjection;
import org.springframework.data.rest.core.config.Projection;

import java.util.Collection;

@Projection(name = "personDoorInfo", types = {Person.class})
public interface ForDoorInfo extends OnlyIdProjection {
    Collection<OnlyUidProjection> getTags();

    Collection<OnlyDocumentProjection> getIdCards();
}