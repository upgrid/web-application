package com.upgrid.presistence.person;

import com.upgrid.presistence.doorLog.DoorLog;
import com.upgrid.presistence.idCard.IdCard;
import com.upgrid.presistence.personGroup.PersonGroup;
import com.upgrid.presistence.tag.Tag;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;

@Entity
@Data
public class Person implements Serializable {

    @Id
    @GeneratedValue
    private long id;
    private String firstName;
    private String lastName;
    private Integer idCode;

    private boolean active;

    @ManyToMany
    private Collection<PersonGroup> personGroups;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "person")
    private Collection<Tag> tags;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "person")
    private Collection<IdCard> idCards;
    @OneToMany(mappedBy = "person")
    private Collection<DoorLog> doorLog;
}