package com.upgrid.presistence.door;

import com.upgrid.presistence.core.OnlyNameProjection;
import org.springframework.data.rest.core.config.Projection;

import java.util.Collection;

@Projection(name = "inlineZones", types = {Door.class})
public interface DoorProjection {
    String getName();

    Collection<OnlyNameProjection> getZones();
}
