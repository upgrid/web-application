package com.upgrid.presistence.door;

import com.upgrid.presistence.core.SearchByNameRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;


@RepositoryRestResource(excerptProjection = DoorProjection.class)
public interface DoorRepository extends SearchByNameRepository<Door, Long> {
}