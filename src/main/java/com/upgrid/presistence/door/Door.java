package com.upgrid.presistence.door;

import com.upgrid.presistence.doorLog.DoorLog;
import com.upgrid.presistence.zone.Zone;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;

@Entity
@Data
public class Door implements Serializable {
    @Id
    @GeneratedValue
    private long id;

    private String name;

    @ManyToMany()
    private Collection<Zone> zones;

    @OneToMany(mappedBy = "door")
    private Collection<DoorLog> doorLog;
}
