package com.upgrid.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * Created by Karl-Mattias on 10.02.2017
 */

@Controller
public class AppController {

    @GetMapping("/")
    public String index() {
        return "landing.html";
    }

    @GetMapping("/access")
    public String access() {
        return "/access/index.html";
    }
}
